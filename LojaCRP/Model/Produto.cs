﻿namespace LojaCRP.DOMINIO.Model
{
    public class Produto : Entidade
    {
        public string Nome { get; set; }
        public decimal Price { get; set; }        

        public Produto() { }
    }
}

﻿using System.Threading.Tasks;
using LojaCRP.DATA;
using LojaCRP.DATA.Repositorio;
using LojaCRP.DOMINIO.Model;
using LojaCRP.WEBAPI.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LojaCRP.WEBAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProdutoController : Controller
    {
        private readonly IRepositorioProduto _repoProd;
        private readonly ApplicationDbContext _aplication;

        public ProdutoController(IRepositorioProduto repoProd, ApplicationDbContext aplication)
        {
            _repoProd = repoProd;
            _aplication = aplication;
        }

        // POST: api/<controller>
        [HttpPost("cadastro")]
        public async Task<IActionResult> Cadastrar([FromBody]ProdutoDto prodDto)
        {
            if (ModelState.IsValid)
            {
                var createProduto = new Produto
                {
                    Nome = prodDto.Nome,
                    Price = prodDto.Price
                };

                var addBase = await _repoProd.CadastrarProduto(createProduto, prodDto.Nome, prodDto.Price);

                return StatusCode(201);
            }

            return BadRequest(ModelState);
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> ListAll()
        {
            var list = await _aplication.dbProduto.ToListAsync();

            return Ok(list);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> ListId(int id)
        {
            var listId = await _aplication.dbProduto.FirstOrDefaultAsync(v => v.Id == id);

            return Ok(listId);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var Delete = await _aplication.dbProduto.FindAsync(id);

            if (Delete != null)
            {
                _aplication.dbProduto.Remove(Delete);
                _aplication.SaveChangesAsync();

                return StatusCode(402);
            }

            return BadRequest(500);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProduto([FromBody]ProdutoDto produtoDto, int id)
        {
            var prod = _aplication.dbProduto.Find(id);

            if(prod != null)
            {
                prod.Nome = produtoDto.Nome;
                prod.Price = produtoDto.Price;

                _aplication.Update(prod);
                _aplication.SaveChangesAsync();
                
                return StatusCode(201);
            }

            return BadRequest(ModelState);
        }
    }
}

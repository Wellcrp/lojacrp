﻿using System.Threading.Tasks;
using LojaCRP.DATA;
using LojaCRP.DATA.Repositorios;
using LojaCRP.DOMINIO.Model;
using LojaCRP.WEBAPI.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LojaCRP.WEBAPI.Controllers
{
    [Route("api/[controller]")]
    public class CategoriaController : Controller
    {
        private readonly IRepositorioCategoria _repoCat;
        private readonly ApplicationDbContext _aplication;

        public CategoriaController(IRepositorioCategoria repoCat, ApplicationDbContext aplication)
        {
            _repoCat = repoCat;
            _aplication = aplication;
        }

        // POST: api/<controller>
        [HttpPost("cadastrar")]
        public async Task<IActionResult> Cadastrar([FromBody]CategoriaDto catDto)
        {
            if (ModelState.IsValid)
            {
                var createCategoria = new Categoria
                {
                    Nome = catDto.Nome
                };

                var addBase = await _repoCat.CadastraCategoria(createCategoria, catDto.Nome);

                return StatusCode(201);
            }

            return BadRequest(ModelState);
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> ListAll()
        {
            var list = await _aplication.dbCategoria.ToListAsync();

            return Ok(list);
        }

        // GET: api/<controller>
        [HttpGet("{id}")]
        public async Task<IActionResult> ListId(int id)
        {
            var listId = await _aplication.dbCategoria.FirstOrDefaultAsync(v => v.Id == id);

            return Ok(listId);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var Delete = await _aplication.dbCategoria.FindAsync(id);

            if(Delete != null)
            {
                _aplication.dbCategoria.Remove(Delete);
                _aplication.SaveChangesAsync();

                return StatusCode(402);
            }
            
            return BadRequest(500);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCategoria([FromBody]CategoriaDto categoriaDto, int id)
        {
            var cat = _aplication.dbCategoria.Find(id);

            if (cat != null)
            {
                cat.Nome = categoriaDto.Nome;                

                _aplication.Update(cat);
                _aplication.SaveChangesAsync();

                return StatusCode(201);
            }

            return BadRequest(ModelState);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LojaCRP.WEBAPI.Dtos
{
    public class CategoriaDto
    {
        public int Id { get; set; }

        [Required]
        public string Nome { get; set; }
    }
}

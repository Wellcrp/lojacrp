﻿using System.ComponentModel.DataAnnotations;

namespace LojaCRP.WEBAPI.Dtos
{
    public class ProdutoDto
    {
        [Required]
        public string Nome { get; set; }        
        public decimal Price { get; set; }        
    }
}

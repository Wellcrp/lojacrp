﻿using LojaCRP.DOMINIO.Model;
using Microsoft.EntityFrameworkCore;

namespace LojaCRP.DATA
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<Produto> dbProduto { get; set; }
        public DbSet<Categoria> dbCategoria { get; set; }
    }
}

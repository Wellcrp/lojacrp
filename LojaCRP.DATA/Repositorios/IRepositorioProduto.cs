﻿using LojaCRP.DOMINIO.Model;
using System.Threading.Tasks;

namespace LojaCRP.DATA.Repositorio
{
    public interface IRepositorioProduto
    {
        Task<Produto> CadastrarProduto(Produto produto, string nome, decimal price);        
    }
}

﻿using LojaCRP.DATA.Repositorio;
using LojaCRP.DOMINIO.Model;
using System.Threading.Tasks;

namespace LojaCRP.DATA.Repositorios
{
    public class RepositorioProduto : IRepositorioProduto
    {
        private readonly ApplicationDbContext _contexto;

        public RepositorioProduto(ApplicationDbContext contexto)
        {
            _contexto = contexto;
        }

        public async Task<Produto> CadastrarProduto(Produto produto, string nome, decimal price)
        {
            produto.Nome = nome;
            produto.Price = price;

            await _contexto.dbProduto.AddAsync(produto);
            await _contexto.SaveChangesAsync();

            return produto;
        }
    }
}

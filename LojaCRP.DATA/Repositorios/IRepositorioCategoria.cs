﻿using LojaCRP.DOMINIO.Model;
using System.Threading.Tasks;

namespace LojaCRP.DATA.Repositorios
{
    public interface IRepositorioCategoria
    {
        Task<Categoria> CadastraCategoria(Categoria categoria, string nome);        
    }
}

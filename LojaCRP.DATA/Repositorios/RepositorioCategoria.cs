﻿using LojaCRP.DOMINIO.Model;
using System.Threading.Tasks;

namespace LojaCRP.DATA.Repositorios
{
    public class RepositorioCategoria : IRepositorioCategoria
    {
        private readonly ApplicationDbContext _contexto;

        public RepositorioCategoria(ApplicationDbContext contexto)
        {
            _contexto = contexto;
        }

        public async Task<Categoria> CadastraCategoria(Categoria categoria, string nome)
        {
            categoria.Nome = nome;

            await _contexto.dbCategoria.AddAsync(categoria);
            await _contexto.SaveChangesAsync();

            return categoria;
        }     
    }
}

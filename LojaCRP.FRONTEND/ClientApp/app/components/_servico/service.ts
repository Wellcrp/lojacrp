import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';

@Injectable()
export class Service {
    baseUrl = 'http://localhost:56877/api/categoria/';

    constructor(private http: Http) { }

    Cadastro(model: any){
        return this.http.post(this.baseUrl + 'cadastrar', model, this.cabecalhoRequisicao());
    }

    private cabecalhoRequisicao(){
        const headers = new Headers({ 'Content-type': 'application/json' });
        return new RequestOptions({ headers: headers });
    }
}
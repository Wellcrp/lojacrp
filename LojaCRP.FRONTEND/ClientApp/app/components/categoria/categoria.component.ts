import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {Service} from '../_servico/service';

@Component({
    selector: 'categoria',
    templateUrl: './categoria.component.html'
})
export class CategoriaComponent implements OnInit {
    model:any = {};

    constructor(private servico: Service) { }

    ngOnInit(){

    }

    Cadastro(){
        this.servico.Cadastro(this.model).subscribe(() => {
            console.log('Cadastro Efedutado');
        }, error => {
            console.log(error);
        });
    }
}

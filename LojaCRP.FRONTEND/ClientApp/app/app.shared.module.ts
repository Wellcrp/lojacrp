import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { CategoriaListarComponent } from './components/categoria/categoriaListar.component';
import { Service } from './components/_servico/service';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,        
        HomeComponent,
        CategoriaComponent,
        CategoriaListarComponent
    ],
    providers:[
        Service
    ],
    imports: [
        CommonModule,
        BrowserModule,
        HttpModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'categoria', component: CategoriaComponent },            
            { path: 'categoriaListar', component: CategoriaListarComponent },            
            { path: '**', redirectTo: 'home' }
        ])
    ]    
})
export class AppModuleShared {
}

﻿using LojaCRP.DATA;
using LojaCRP.DATA.Repositorio;
using LojaCRP.DATA.Repositorios;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace LojaCRP.ID
{
    public class Dependencia
    {
        public static void Configure(IServiceCollection services, string connection)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connection));
            
            services.AddTransient(typeof(IRepositorioCategoria), typeof(RepositorioCategoria));
            services.AddTransient(typeof(IRepositorioProduto), typeof(RepositorioProduto));
        }
    }
}
